package andre.se.kth.id1212.common;

public enum State {
    INPROGRESS,
    VICTORY,
    GAMEOVER
}