package andre.se.kth.id1212.common;

public enum MsgType {
    START,
    DISCONNECT,
    LETTER,
    WORD,
    GAME,
}
