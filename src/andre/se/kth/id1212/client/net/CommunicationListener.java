package andre.se.kth.id1212.client.net;

import andre.se.kth.id1212.common.Message;

import java.net.InetSocketAddress;

public interface CommunicationListener {
    /**
     * Called when a broadcast message from the server has been received. That message originates
     * from one of the clients.
     *
     * @param msg The message from the server.
     */
    public void receivedMsg(Message msg);

    /**
     * Called when the local client is successfully connected to the server.
     *
     * @param serverAddress The address of the server to which connection is established.
     */
    public void connected(InetSocketAddress serverAddress);

    /**
     * Called when the local client is successfully disconnected from the server.
     */
    public void disconnected();
}
