package andre.se.kth.id1212.client.controller;

import andre.se.kth.id1212.client.net.CommunicationListener;
import andre.se.kth.id1212.client.net.ServerConnection;
import andre.se.kth.id1212.common.MsgType;

import java.io.IOException;

public class Controller {
    private final ServerConnection serverConnection = new ServerConnection();

    public void connect(String host, int port, CommunicationListener communicationListener) {
        serverConnection.addCommunicationListener(communicationListener);
        serverConnection.connect(host, port);
    }

    private void disconnect() throws IOException {
        serverConnection.disconnect();
    }

    public boolean parseCommand(String command) throws IOException {
        try {
            switch (command.toUpperCase().charAt(0)) {
                case 'S':
                    sendMsg(MsgType.START, null);
                    return true;
                case 'L':
                    sendMsg(MsgType.LETTER, command);
                    return true;
                case 'W':
                    sendMsg(MsgType.WORD, command);
                    return true;
                case 'Q':
                    sendMsg(MsgType.DISCONNECT, null);
                    disconnect();
                    return false;
                default:
                    System.out.println("Wrong command, try again.");
                    return true;
            }
        } catch (StringIndexOutOfBoundsException se) {
            System.out.println("Wrong command, try again.");
            return true;
        }
    }

    private void sendMsg(MsgType type, String msg) throws IOException {
        switch (type) {
            case START:
                serverConnection.requestNewGame();
                break;
            case LETTER:
                serverConnection.sendCharacterGuess(String.valueOf(msg.charAt(2)));
                break;
            case WORD:
                serverConnection.sendWordGuess(msg.substring(2));
                break;
            case DISCONNECT:
                serverConnection.disconnect();
                break;
        }
    }

}
