package andre.se.kth.id1212.client.view;

import andre.se.kth.id1212.client.controller.Controller;
import andre.se.kth.id1212.client.net.CommunicationListener;
import andre.se.kth.id1212.common.Message;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Scanner;

public class NonBlockingInterpreter implements Runnable {
    private static final String PROMPT = "> ";
    private final Scanner console = new Scanner(System.in);
    private final ThreadSafeStdOut outMgr = new ThreadSafeStdOut();
    private boolean receivingCmds = false;
    private Controller controller;
    /**
     * Starts the interpreter. The interpreter will be waiting for user input when this method
     * returns. Calling <code>start</code> on an interpreter that is already started has no effect.
     */
    public void start() {
        if (receivingCmds) {
            return;
        }
        receivingCmds = true;
        controller = new Controller();
        controller.connect("127.0.0.1", 4444, new ConsoleOutput());
        new Thread(this).start();
    }

    /**
     * Interprets and performs user commands.
     */
    @Override
    public void run() {
        while (receivingCmds) {
            try {
                String command = readNextLine();
                receivingCmds = controller.parseCommand(command);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String readNextLine() {
        outMgr.print(PROMPT);
        return console.nextLine();
    }

    private class ConsoleOutput implements CommunicationListener {
        @Override
        public void receivedMsg(Message msg) {
            ArrayList<String> toPrint = parseMessage(msg);
            for(String print : toPrint)
                outMgr.println(print);
            outMgr.print(PROMPT);
        }

        @Override
        public void connected(InetSocketAddress serverAddress) {
            printToConsole("Connected to " + serverAddress.getHostName() + ":"
                    + serverAddress.getPort());
        }

        @Override
        public void disconnected() {
            printToConsole("Disconnected from server.");
        }

        private void printToConsole(String output) {
            outMgr.println(output);
            outMgr.print(PROMPT);
        }

        private ArrayList<String> parseMessage(Message msg) {
            ArrayList<String> msgArray = new ArrayList<>();
            switch (msg.getType()) {
                case GAME:
                    switch (msg.getGameState().getState()) {
                        case GAMEOVER:
                            msgArray.add("GAME OVER! The word was " + msg.getGameState().getCorrectLetters());
                            msgArray.add("Current score: " + msg.getGameState().getCurrentScore());
                            msgArray.add("Type 'S' to start new game.");
                            return msgArray;
                        case VICTORY:
                            msgArray.add("CORRECT! The word was " + msg.getGameState().getCorrectLetters());
                            msgArray.add("Current score: " + msg.getGameState().getCurrentScore());
                            msgArray.add("Type 'S' to start new game.");
                            return msgArray;
                        case INPROGRESS:
                            StringBuilder correct = new StringBuilder(msg.getGameState().getCorrectLetters());
                            for (int i = 0; i < correct.length(); i++)
                                if (i % 2 == 1) correct.insert(i, ' ');
                            msgArray.add("CURRENT GAME: ");
                            msgArray.add("Current score: " + msg.getGameState().getCurrentScore() + "  |  Attempts left: " + msg.getGameState().getAttemptsLeft());
                            msgArray.add("Guessed characters: " + msg.getGameState().getGuessedLetters());
                            msgArray.add(correct.toString());
                            msgArray.add(" ");
                            return msgArray;
                    }
                case DISCONNECT:
                    msgArray.add("SERVER SENT DISCONNECT");
                    return msgArray;
            }
            return msgArray;
        }

    }
}
