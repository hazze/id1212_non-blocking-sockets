package andre.se.kth.id1212.client.startup;

import andre.se.kth.id1212.client.view.NonBlockingInterpreter;

public class Client {
    public static void main(String[] args) {
        new NonBlockingInterpreter().start();
    }
}
