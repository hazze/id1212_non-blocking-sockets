package andre.se.kth.id1212.server.controller;

import andre.se.kth.id1212.common.GameState;
import andre.se.kth.id1212.common.Message;
import andre.se.kth.id1212.common.MsgType;
import andre.se.kth.id1212.common.State;
import andre.se.kth.id1212.server.model.Game;
import andre.se.kth.id1212.server.model.Word;

import java.util.HashMap;

public class Controller implements GameParser {
    private final Word words;
    private HashMap<Integer, Game> gameList = new HashMap<>();


    /**
     * Creates an instance of the controller containing the reference to the list of words.
     */
    public Controller() {
        this.words = new Word();
    }

    /**
     * Parses all incoming messages and handles them accordingly.
     * @param msg The message received that will be parsed.
     * @return The message that will be sent back to the sender of the parsed message.
     */
    public Message parseMessage(Message msg, Integer id) {
        Game currentGame;
        switch (msg.getType()) {
            case START:
                currentGame = createNewGame(gameList.get(id) == null ? 0 : gameList.get(id).getCurrentScore());
                gameList.put(id, currentGame);
                return new Message(MsgType.GAME, null, generateGameState(currentGame, State.INPROGRESS));
            case LETTER:
                if ((currentGame = getCurrentGame(id)) != null) {
                    if (currentGame.getAttemptsLeft() == 0 && !(currentGame.getWord().equals(currentGame.getCorrectLetters().toString()))) {
                        currentGame.decreaseScore();
                        return new Message(MsgType.GAME, null, generateGameState(currentGame, State.GAMEOVER));
                    }
                    return new Message(MsgType.GAME, null, checkCharacterGuess(currentGame, msg.getMessage().charAt(0)));
                }
                break;
            case WORD:
                if ((currentGame = getCurrentGame(id)) != null) {
                    if (currentGame.getAttemptsLeft() == 0 && !(currentGame.getWord().equals(currentGame.getCorrectLetters().toString()))) {
                        currentGame.decreaseScore();
                        return new Message(MsgType.GAME, null, generateGameState(currentGame, State.GAMEOVER));
                    }
                    return new Message(MsgType.GAME, null, checkWordGuess(currentGame, msg.getMessage()));
                }
                break;
            case DISCONNECT:
                System.out.println("Client disconnected.");
                return new Message(MsgType.DISCONNECT, null, null);
        }
        return new Message(MsgType.DISCONNECT, null, null);
    }

    private Game createNewGame(int score) {
        return new Game(score, words.getRandomWord());
    }

    private GameState generateGameState(Game game, State state) {
        return new GameState(game.getCorrectLetters(), game.getGuessedLetters(), game.getAttemptsLeft(), game.getCurrentScore(), state);
    }

    private Game getCurrentGame(Integer threadId) {
        return gameList.get(threadId);
    }

    private GameState checkCharacterGuess(Game game, char c) {
        if (game.getAttemptsLeft() > 0 && !(game.getWord().equals(game.getCorrectLetters().toString()))) {
            if (game.getWord().indexOf(c) != -1) {
                int i = game.getWord().indexOf(c);
                while (i >= 0) {
                    game.setCorrectLetters(c, i);
                    i = game.getWord().indexOf(c, i + 1);
                }
            } else {
                if (game.getGuessedLetters().indexOf(c) == -1) {
                    game.setGuessedLetters(c);
                    game.decreaseAttemptsLeft();
                    if (game.getAttemptsLeft() == 0) {
                        game.setCorrectLetters(game.getWord());
                        return generateGameState(game, State.GAMEOVER);
                    }
                }
            }
            if (game.getWord().equals(game.getCorrectLetters().toString())) {
                game.incrementScore();
                game.setAttemptsLeft(0);
                return generateGameState(game, State.VICTORY);
            }
            else {
                return generateGameState(game, State.INPROGRESS);
            }
        }
        game.setCorrectLetters(game.getWord());
        return generateGameState(game, State.GAMEOVER);
    }

    private GameState checkWordGuess(Game game, String word) {
        if (game.getAttemptsLeft() > 0) {
            if (word.equals(game.getWord())) {
                game.setCorrectLetters(word);
                game.incrementScore();
                game.setAttemptsLeft(0);
                return generateGameState(game, State.VICTORY);
            } else {
                game.decreaseAttemptsLeft();
                if (game.getAttemptsLeft() == 0) {
                    game.setCorrectLetters(game.getWord());
                    return generateGameState(game, State.GAMEOVER);
                }
                return generateGameState(game, State.INPROGRESS);
            }
        }
        game.setCorrectLetters(game.getWord());
        return generateGameState(game, State.GAMEOVER);
    }
}
