package andre.se.kth.id1212.server.controller;

import andre.se.kth.id1212.common.Message;

public interface GameParser {
    Message parseMessage(Message msg, Integer id);
}
