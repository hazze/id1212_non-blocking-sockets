package andre.se.kth.id1212.server.net;

import andre.se.kth.id1212.common.Constants;
import andre.se.kth.id1212.common.Message;
import andre.se.kth.id1212.common.MessageSplitter;
import andre.se.kth.id1212.server.controller.Controller;
import andre.se.kth.id1212.server.model.SessionId;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;

/**
 * Receives chat messages and broadcasts them to all chat clients. All communication to/from any
 * chat node passes this server.
 */
public class GameServer {
    private static final int LINGER_TIME = 5000;
    private final Controller controller = new Controller();
    private int portNo = 4444;
    private Selector selector;
    private ServerSocketChannel listeningSocketChannel;
    private HashMap<Integer, SelectionKey> clientIds = new HashMap<>();

    void sendGameMessage(Message msg, SessionId id) throws IOException {
        SelectionKey key = clientIds.get(id.getId());
        ByteBuffer gameMessage = createGameMessage(msg);
        appendToClientQueue(gameMessage, key);
        sendToClient(key);
    }

    private void appendToClientQueue(ByteBuffer msg, SelectionKey key) {
        Client client = (Client) key.attachment();
        synchronized (client.messagesToSend) {
            client.queueMsgToSend(msg);
        }
    }

    private ByteBuffer createGameMessage(Message msg) {
        StringJoiner joiner = new StringJoiner(Constants.MSG_TYPE_DELIMETER);
        joiner.add(msg.getType().toString());
        joiner.add(msg.getGameState().toString());
        String messageWithLengthHeader = MessageSplitter.prependLengthHeader(joiner.toString());
        return ByteBuffer.wrap(messageWithLengthHeader.getBytes());
    }

    private void serve() {
        try {
            initSelector();
            initListeningSocketChannel();
            while (true) {
                selector.select();
                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    iterator.remove();
                    if (!key.isValid()) {
                        continue;
                    }
                    if (key.isAcceptable()) {
                        startHandler(key);
                    } else if (key.isReadable()) {
                        recvFromClient(key);
                    } else if (key.isWritable()) {
                        sendToClient(key);
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Server failure.");
        }
    }

    private void startHandler(SelectionKey key) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
        SocketChannel clientChannel = serverSocketChannel.accept();
        clientChannel.configureBlocking(false);
        SessionId id = new SessionId();
        ClientHandler handler = new ClientHandler(this, clientChannel, controller, id);
        clientIds.put(id.getId(), clientChannel.register(selector, SelectionKey.OP_WRITE, new Client(handler)));
        clientChannel.setOption(StandardSocketOptions.SO_LINGER, LINGER_TIME);
    }

    private void recvFromClient(SelectionKey key) throws IOException {
        Client client = (Client) key.attachment();

        try {
            client.handler.receivedMsg();
        } catch (IOException clientHasClosedConnection) {
            removeClient(key);
        }
    }

    private void sendToClient(SelectionKey key) throws IOException {
        Client client = (Client) key.attachment();
        try {
            client.sendAll();
            key.interestOps(SelectionKey.OP_READ);
        } catch (IOException clientHasClosedConnection) {
            removeClient(key);
        }
    }

    private void removeClient(SelectionKey key) throws IOException {
        Client client = (Client) key.attachment();
        client.handler.disconnectClient();
        key.cancel();
    }

    private void initSelector() throws IOException {
        selector = Selector.open();
    }

    private void initListeningSocketChannel() throws IOException {
        listeningSocketChannel = ServerSocketChannel.open();
        listeningSocketChannel.configureBlocking(false);
        listeningSocketChannel.bind(new InetSocketAddress(portNo));
        listeningSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
    }

    private class Client {
        private final ClientHandler handler;
        private final Queue<ByteBuffer> messagesToSend = new ArrayDeque<>();

        private Client(ClientHandler handler) {
            this.handler = handler;
        }

        private void queueMsgToSend(ByteBuffer msg) {
            System.out.println(msg.toString());
            synchronized (messagesToSend) {
                messagesToSend.add(msg.duplicate());
            }
        }

        private void sendAll() throws IOException {
            ByteBuffer msg;
            synchronized (messagesToSend) {
                while ((msg = messagesToSend.peek()) != null) {
                    handler.sendMsg(msg);
                    messagesToSend.remove();
                }
            }
        }
    }

    public static void main(String[] args) {
        GameServer server = new GameServer();
        server.serve();
    }
}