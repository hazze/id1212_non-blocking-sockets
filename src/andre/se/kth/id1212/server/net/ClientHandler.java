package andre.se.kth.id1212.server.net;

import andre.se.kth.id1212.common.*;
import andre.se.kth.id1212.server.controller.GameParser;
import andre.se.kth.id1212.server.model.SessionId;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ForkJoinPool;


/**
 * Handles all communication with one particular chat client.
 */
public class ClientHandler implements Runnable {
    private final GameServer server;
    private final SocketChannel clientChannel;
    private final GameParser gameParser;
    private final SessionId key;
    private final ByteBuffer msgFromClient = ByteBuffer.allocateDirect(Constants.MAX_MSG_LENGTH);
    private final MessageSplitter msgSplitter = new MessageSplitter();

    /**
     * Creates a new instance, which will handle communication with one specific client connected to
     * the specified channel.
     *
     * @param server The GameServer reference
     * @param clientChannel The socket to which this handler's client is connected.
     * @param gameParser The Game parser reference
     * @param key The id for the client session.
     */
    ClientHandler(GameServer server, SocketChannel clientChannel, GameParser gameParser, SessionId key) {
        this.server = server;
        this.clientChannel = clientChannel;
        this.gameParser = gameParser;
        this.key = key;
    }

    /**
     * Receives and handles one message from the connected client.
     */
    @Override
    public void run() {
        while (msgSplitter.hasNext()) {
            try {
                server.sendGameMessage(gameParser.parseMessage(new MessageCH(msgSplitter.nextMsg()).createMessageObject(), key.getId()), key);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Sends the specified message to the connected client.
     *
     * @param msg The message to send.
     * @throws IOException If failed to send message.
     */
    public void sendMsg(ByteBuffer msg) throws IOException {
        clientChannel.write(msg);
    }

    /**
     * Reads a message from the connected client, then submits a task to the default
     * <code>ForkJoinPool</code>. That task which will handle the received message.
     *
     * @throws IOException If failed to read message
     */
    void receivedMsg() throws IOException {
        msgFromClient.clear();
        int numOfReadBytes;
        numOfReadBytes = clientChannel.read(msgFromClient);
        if (numOfReadBytes == -1) {
            throw new IOException("Client has closed connection.");
        }
        msgSplitter.appendRecvdString(extractMessageFromBuffer());
        ForkJoinPool.commonPool().execute(this);
    }

    private String extractMessageFromBuffer() {
        msgFromClient.flip();
        byte[] bytes = new byte[msgFromClient.remaining()];
        msgFromClient.get(bytes);
        return new String(bytes);
    }

    /**
     * Closes this instance's client connection.
     * @throws IOException If failed to close connection.
     */
    void disconnectClient() throws IOException {
        clientChannel.close();
    }

    private static class MessageCH {
        private MsgType msgType;
        private String msgBody;

        private MessageCH(String receivedString) {
            parse(receivedString);
        }

        private void parse(String strToParse) {
            try {
                String[] msgTokens = strToParse.split(Constants.MSG_TYPE_DELIMETER);
                msgType = MsgType.valueOf(msgTokens[Constants.MSG_TYPE_INDEX].toUpperCase());
                if (hasBody(msgTokens)) {
                    msgBody = msgTokens[Constants.MSG_BODY_INDEX].trim();
                }
            } catch (Throwable throwable) {
                System.err.println("Error parsing message (MessageCH).");
            }
        }

        private boolean hasBody(String[] msgTokens) {
            return msgTokens.length > 1;
        }

        /**
         * @return A message object from the parsed message for easier handling.
         */
        Message createMessageObject() {
            return new Message(msgType, msgBody, null);
        }
    }
}