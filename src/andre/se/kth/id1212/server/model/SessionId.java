package andre.se.kth.id1212.server.model;

/**
 * Class to contain the id for each client session.
 */
public class SessionId {
    private static int ID_COUNTER = 0;
    private final Integer id = ID_COUNTER++;

    /**
     * @return returns the current sessions's id.
     */
    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "SessionId{" +
                "id=" + id +
                '}';
    }
}
